# Dark Chat

Chat para redes LAN por consola diseñado. Fue diseñado con el fin de hacer presentaciones con estilo hacker y/o DepWeb. 

![demo](img_readme/demo.PNG)

Para un mejor efecto se recomienda modificar la consola CMD o PowerShell 


![config_cmd](img_readme/config_cemd.png)

## Instalación Windows

### Python

Python: Lenguaje requerido para ejecutar en modo de depuración y crear instaladores .EXE [Link de descarga](https://www.python.org/ftp/python/3.12.4/python-3.12.4-amd64.exe)

Se recomienda el uso de enotrno virtual. Una vez instalado Python ejecutar en una consola CMD
```
pip install virtualenv
```

### GIT
GIT: Software de versionamiento necesario para actualizar el proyecto. [Link de descarga.](https://github.com/git-for-windows/git/releases/download/v2.45.0.windows.1/Git-2.45.0-64-bit.exe)

Puede consultar la [documentacion GIT](https://git-scm.com/doc)

Curso express [Youtube Git](https://youtu.be/VdGzPZ31ts8?si=6_1L4hqONhiG1Qf0)

### Se recomienda configurar el [acceso ssh al repositorio GIT](https://docs.gitlab.com/ee/user/ssh.html), a fin de no solicitar contraseñas en cada peticion push / pull / fecth

Una vez instalado GIT:

Realizar clone al repositorio (configurado ssh):

```
git clone git@gitlab.com:david.jimenez3/darkchat.git
cd darkchat
```
O si no tiene configurado ssh:
```
git clone https://gitlab.com/david.jimenez3/darkchat.git
cd darkchat

```

### Entorno Virtual python
Para crear un entorno virtual ejecutar:
```
python -m venv .venv
```
Una vez creado debe activarse ejecutando:
```
.venv\Scripts\activate

```
Al activar el entorno virtual la consola antepondra a al directorio (.venv)
### Instalar requerimientos
  

```
pip install -r src\requirements.txt

```
### Ejecutar

1. Ejecutar servidor
```
python  src\server.py
```
Debe ingresar al ip del equipo y un puerto disponible
2. Ejecutar cliente
```
python  src\cliente.py
```
Deb ingresa rel mism po puero e ip del servidor




## VISUAL STUDIO CODE
Descar [VS code](https://code.visualstudio.com/download)

Instalar extension [python debug para VS code](https://marketplace.visualstudio.com/items?itemName=ms-python.python)

Abrir la carpeta de darkchat con VS Code

Iniciar el modo [debug](https://code.visualstudio.com/docs/editor/debugging)

Al seleccionar "create a launch,json file seleccionar la opción Python Debbuger y luego Pyhton file

Se creará el archivo .vscode/launch.json 

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python Debugger: Current File",
            "type": "debugpy",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal"
        }
    ]
}
```
En la nueva barra lateal de debug, en la parte superior encontrá la opcion RUn adn Debug, ejecutar con el icono de Play o F5

### Crear instalador
Para crear archivos .exe de el cliente y servidor debe ejecutar lo siguiente:
```
cd ejecutables
pyinstaller --onefile -w ../src/client.py
pyinstaller --onefile -w ../src/server.py
```
Los ejecutables generados se ubican en la carpeta ejecutables/dist.

