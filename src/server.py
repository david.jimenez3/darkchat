
import socket
import threading
import sys

try:
    host = input("Ingrese IP: ")  # LocalHost
    # Puerto no reservado
    port = int(input("Ingrese puerto: "))
except Exception as e:
    print(f'Error {e}')
    sys.exit()

# inicia socket
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((host, port))  # binding host y puerto para el socket
server.listen()

clients = []
nicknames = []


def broadcast(message):
    for client in clients:
        client.send(message)


def handle(client):
    salir = True
    while salir:
        try:  # recibe mensajes del cliente
            message = client.recv(1024)
            exit_message = message.decode('utf-8').upper()
            if 'ADIOS' not in exit_message:
                broadcast(message)
            else:
                exit(client)
                salir = False
                break
        except Exception as e:  # removing clients
            print(f"error {e}")
            exit(client)
            salir = False


def exit(client):
    index = clients.index(client)
    nickname = nicknames[index]
    broadcast('{} EXIT Pronto sabras de nosotros!!!'.format(
        nickname).encode('utf-8'))

    clients.remove(client)
    client.close()
    nicknames.remove(nickname)


def receive():  # Recibve clientes
    while True:
        client, address = server.accept()
        print("Conectando con {}".format(str(address)))
        client.send('NICKNAME'.encode('utf-8'))
        nickname = client.recv(1024).decode('utf-8')
        nicknames.append(nickname)
        clients.append(client)
        print("Tu nombre es {}".format(nickname))
        broadcast("Validando su identidad {}. Espere...".format(
            nickname).encode('utf-8'))
        client.send('Conectado al chat!'.encode('utf-8'))
        thread = threading.Thread(target=handle, args=(client,))
        thread.start()


receive()
