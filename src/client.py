import socket
import threading
import sys
import time
import random
from progress.bar import ChargingBar


try:
    host = input("Ingrese IP: ")  # LocalHost
    port = int(input("Ingrese puerto, solo numeros: "))  # Puerto no reservado
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((host, port))  # Conectando cliente con servidor
    print(f'Conectado a {host}:{port}'.center(50, '='))

except Exception as e:

    print(f'Error de conexión {e.args} ')
    sys.exit()


# Efecto encriptado
key = input("Ingrese llave de seguridad: ")
bar1 = ChargingBar(f"Encriptando llave {key}", max=100)
for num in range(100):
    time.sleep(random.uniform(0, 0.1))
    bar1.next()
bar1.finish()
print('Conexión segura,puedes hablar')

nickname = input("Nos has contactado,  ¿como te llamas?: ")
# Inica socket


def receive():
    while True:  # haciendo una conexión valida
        try:
            message = client.recv(1024).decode('utf-8')
            if message == 'NICKNAME':
                client.send(nickname.encode('utf-8'))
            elif 'EXIT' in message:
                print(message)
                client.close()
                sys.exit()
            else:
                print(message)
        except Exception as e:  # Erro en caso de falla ip/puerto
            print(f"Error {e}")
            client.close()
            sys.exit()


def write():
    while True:  # Mensajes
        message = '{}: {}'.format(nickname, input(''))
        client.send(message.encode('utf-8'))


receive_thread = threading.Thread(
    target=receive)  # Recibe varios mensajes
receive_thread.start()
write_thread = threading.Thread(target=write)  # envia mensajes
write_thread.start()
